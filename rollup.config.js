import buble from '@rollup/plugin-buble';
import vue from 'rollup-plugin-vue';

export default {
  input: 'src/js/index.js',
  output: {
    format: 'esm',
    dir: 'dist',
  },
  plugins: [
    vue(),
  ]
}