import 'vue';
import { EventBus } from './index.js';

var script = {
    inject: ['dialogId'],

    async mounted () {
        this.$el.addEventListener('click', this.trigger);
    },

    methods: {
        trigger () {
            EventBus.$emit('closeDialog', this.dialogId);
        }
    },

    render () {
        return this.$scopedSlots.default();
    }
};

export default script;
