import Vue from 'vue';

const EventBus = new Vue();

const components = {
    DialogProvider: () =>
        import(
            /* webpackChunkName: "dialog-provider" */ './DialogProvider-1b2569e6.js'
        ),
    DialogTriggerProvider: () =>
        import(
            /* webpackChunkName: "dialog-trigger-provider" */ './DialogTriggerProvider-3d783dd3.js'
        ),
    DialogCloseProvider: () =>
        import(
            /* webpackChunkName: "dialog-close-provider" */ './DialogCloseProvider-301b3932.js'
        ),
};

var index = (Vue) => {
    for (const name in components) {
        Vue.component(name, components[name]);
    }
};

export default index;
export { EventBus };
