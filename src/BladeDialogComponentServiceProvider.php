<?php

namespace Bravissimo\BladeDialogComponent;

class BladeDialogComponentServiceProvider {
    public function __construct() {
        $views = config('app.view_paths');
        $views[] = get_template_directory() . '/vendor/bravissimo_agency/blade-dialog-component/src';

        app('config')->set('app.view_paths', $views);
    }

    public function boot() {
        addBladeComponents([
            'components.dialog' => 'dialog',
        ]);
    }
}
